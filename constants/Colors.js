export default {
	primary: '#8F79FC',
	primaryFaded: 'rgba(169, 149, 244, .1)',
	secondary: '#FBFAFF',
	backForm: '#2c3e50',
	loginButton: '#2552ff',
};
