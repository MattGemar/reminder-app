// Librairies
import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	SafeAreaView,
	Platform,
	TouchableOpacity,
	TextInput,
} from 'react-native';
import Colors from '../constants/Colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useForm, Controller } from 'react-hook-form';
import * as appActions from '../store/actions/app';

// Redux
import { useDispatch, useSelector } from 'react-redux';

export default function AddNote(props) {
	//Variables
	const {
		control,
		handleSubmit,
		formState: { errors },
	} = useForm();
	const project = props.route.params.project;
	const dispatch = useDispatch();
	const userId = useSelector((state) => state.userId);
	const token = useSelector((state) => state.token);

	// Fonctions
	const onSubmit = (data) => {
		const note = {
			projectId: project.id,
			content: data.content,
			creationDate: new Date(),
		};
		dispatch(appActions.addNote(note, userId, token));
		props.navigation.goBack();
	};

	return (
		<View style={styles.container}>
			<SafeAreaView style={{ flex: 1 }}>
				<Text style={styles.title}>Ajouter une note</Text>
				<View style={styles.inputContainer}>
					<Text style={styles.projectName}>{project.name}</Text>
					<Controller
						control={control}
						render={({ field: { value, onChange } }) => (
							<TextInput
								style={styles.input}
								placeholder='Tapez quelque chose...'
								value={value}
								onChangeText={(value) => onChange(value)}
								multiline={true}
							/>
						)}
						name='content'
						rules={{
							required: true,
						}}
					/>
				</View>
				<TouchableOpacity
					activeOpacity={0.8}
					style={styles.submit}
					onPress={handleSubmit(onSubmit)}>
					<Text style={styles.submitText}>Ajouter</Text>
					<Ionicons name='arrow-forward' size={23} color='white' />
				</TouchableOpacity>
				<TouchableOpacity
					activeOpacity={0.8}
					style={styles.close}
					onPress={() => props.navigation.goBack()}>
					<Ionicons name='close' size={23} color='white' />
				</TouchableOpacity>
			</SafeAreaView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: Colors.primaryFaded,
		paddingHorizontal: 25,
	},
	title: {
		fontSize: 30,
		fontWeight: 'bold',
		marginBottom: 30,
		marginTop: Platform.OS === 'android' ? 50 : 30,
		alignSelf: 'center',
	},
	close: {
		backgroundColor: Colors.primary,
		height: 50,
		width: 50,
		borderRadius: 25,
		alignItems: 'center',
		justifyContent: 'center',
		alignSelf: 'center',
		position: 'absolute',
		bottom: 50,
	},
	inputContainer: {
		backgroundColor: 'white',
		padding: 15,
		borderRadius: 15,
		shadowColor: '#000000',
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.2,
		shadowRadius: 1.41,
		elevation: 2,
	},
	input: {
		maxHeight: 150,
		fontSize: 16,
	},
	submit: {
		backgroundColor: Colors.primary,
		padding: 10,
		width: 130,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		alignSelf: 'center',
		marginTop: 30,
		borderRadius: 10,
	},
	submitText: {
		color: 'white',
		fontSize: 17,
	},
	projectName: {
		fontWeight: 'bold',
		color: Colors.primary,
		marginBottom: 5,
	},
});
