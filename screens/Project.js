// Librairies
import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	SafeAreaView,
	TouchableOpacity,
	Platform,
	Image,
	FlatList,
} from 'react-native';
import Colors from '../constants/Colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { LinearGradient } from 'expo-linear-gradient';

// Composants
import Note from '../components/Note/Note';

// Redux
import { useSelector, useDispatch } from 'react-redux';

export default function Project(props) {
	// Variables
	const project = props.route.params.item;
	const notes = useSelector((state) => state.notes).filter(
		(note) => note.projectId === project.id
	);
	return (
		<View style={styles.container}>
			<SafeAreaView style={{ flex: 1 }}>
				<TouchableOpacity
					activeOpacity={0.8}
					onPress={() => props.navigation.goBack()}>
					<View style={styles.backButton}>
						<Ionicons name='arrow-back' size={23} color='white' />
					</View>
				</TouchableOpacity>
				<FlatList
					data={notes}
					renderItem={({ item }) => <Note item={item} />}
					ListHeaderComponent={() => (
						<>
							<View style={styles.informations}>
								<Image
									source={
										project.logo
											? { uri: project.logo }
											: require('../assets/placeHolderLogo.jpeg')
									}
									style={styles.logo}
								/>
								<Text style={styles.title}>{project.name}</Text>
							</View>
							{notes[0] ? (
								<>
									<TouchableOpacity
										activeOpacity={0.8}
										onPress={() =>
											props.navigation.navigate('addNote', { project: project })
										}>
										<View style={styles.smallAddButton}>
											<Text style={styles.smallAddButtonText}>
												Ajouter une note
											</Text>
										</View>
									</TouchableOpacity>
								</>
							) : (
								<>
									<Image
										source={require('../assets/empty.png')}
										style={styles.image}
									/>
									<Text>
										Commencez par ajouter votre première note pour ce projet.
									</Text>
									<TouchableOpacity
										activeOpacity={0.8}
										onPress={() =>
											props.navigation.navigate('addNote', { project: project })
										}>
										<LinearGradient
											colors={['#A996F2', '#8F79FC']}
											style={styles.addButton}>
											<Text style={styles.addButtonText}> Créer une note </Text>
										</LinearGradient>
									</TouchableOpacity>
								</>
							)}
						</>
					)}
				/>
			</SafeAreaView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: Colors.secondary,
		paddingHorizontal: 25,
	},
	title: {
		fontSize: 30,
		fontWeight: 'bold',
		marginTop: 30,
		color: 'white',
	},
	backButton: {
		backgroundColor: Colors.primary,
		height: 30,
		width: 30,
		borderRadius: 15,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: Platform.OS === 'android' ? 50 : 30,
	},
	image: {
		width: 350,
		height: 200,
	},
	logo: {
		width: 150,
		height: 150,
		borderRadius: 75,
		alignSelf: 'center',
		marginBottom: 30,
	},
	informations: {
		backgroundColor: Colors.primary,
		marginTop: 15,
		borderRadius: 15,
		padding: 30,
		alignItems: 'center',
	},
	addButton: {
		padding: 10,
		borderRadius: 5,
		marginTop: 30,
		alignItems: 'center',
	},
	addButtonText: {
		fontSize: 18,
		color: 'white',
	},
	smallAddButton: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 30,
		width: 140,
		borderRadius: 15,
		backgroundColor: Colors.primary,
		marginVertical: 30,
		alignSelf: 'flex-end',
	},
	smallAddButtonText: {
		color: 'white',
	},
});
