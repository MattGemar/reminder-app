// Librairies
import React, { useEffect } from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as appActions from '../store/actions/app';
import Colors from '../constants/Colors';

// Redux
import { useDispatch } from 'react-redux';

export default function Startup(props) {
	// Variables
	const dispatch = useDispatch();

	// Cycle de vie
	useEffect(() => {
		const tryLogin = async () => {
			const userData = await AsyncStorage.getItem('userData');
			if (!userData) {
				dispatch(appActions.setDidTrial());
				return;
			}
			const transformedData = JSON.parse(userData);
			const { token, refreshToken } = transformedData;

			if (!token || !refreshToken) {
				dispatch(appActions.setDidTrial());
				return;
			}

			dispatch(appActions.fetchRefreshToken(refreshToken));
		};
		tryLogin();
	}, []);

	return (
		<View style={styles.container}>
			<ActivityIndicator size='large' color={Colors.primary} />
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
});
