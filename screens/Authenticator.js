// Librairies
import React, { useState } from 'react';
import {
	StyleSheet,
	Text,
	View,
	SafeAreaView,
	Platform,
	TouchableOpacity,
	TextInput,
	Dimensions,
	KeyboardAvoidingView,
	Alert,
} from 'react-native';
import { useForm, Controller } from 'react-hook-form';
import Colors from '../constants/Colors';

import * as appActions from '../store/actions/app';

// Redux
import { useDispatch, useSelector } from 'react-redux';

export default function Authenticator(props) {
	// Variables
	const {
		control,
		handleSubmit,
		formState: { errors },
	} = useForm();
	const dispatch = useDispatch();
	const isAuth = useSelector((state) => state.userId);

	console.log(isAuth);

	// State
	const [loginMode, setLoginMode] = useState(false);

	// Fonctions
	const onSubmit = async (data) => {
		const user = {
			email: data.email,
			password: data.password,
		};
		console.log(user);
		if (loginMode) {
			//connexion
			try {
				await dispatch(appActions.login(user.email, user.password));
				props.navigation.navigate('app');
			} catch (error) {
				switch (error.message) {
					case 'EMAIL_NOT_FOUND':
						Alert.alert(
							'impossible de vous connecter',
							"Cette adresse email n'est lié à aucun compte."
						);
						break;
					case 'INVALID_PASSWORD':
						Alert.alert(
							'impossible de vous connecter',
							'Votre mot de passe est incorrect.'
						);
						break;
					case 'USER_DISABLED':
						Alert.alert(
							'impossible de vous connecter',
							'Votre compte est désa.'
						);
						break;
					default:
						Alert.alert(
							'impossible de vous connecter',
							'Une erreur est survenue.'
						);
						break;
				}
			}
		} else {
			// Inscription
			try {
				await dispatch(appActions.signup(user.email, user.password));
				props.navigation.navigate('app');
			} catch (error) {
				switch (error.message) {
					case 'EMAIL_EXISTS':
						Alert.alert(
							'impossible de vous inscrire',
							'Cette adresse email est déjà utilisée sur un autre compte.'
						);
						break;
					default:
						Alert.alert(
							'impossible de vous inscrire',
							'Une erreur est survenue.'
						);
						break;
				}
			}
		}
	};

	return (
		<KeyboardAvoidingView
			style={{ flex: 1 }}
			behavior={Platform.OS === 'android' ? 'height' : 'padding'}>
			<View style={styles.container}>
				<SafeAreaView style={{ flex: 1 }}>
					<View style={styles.littleContainer}>
						<Text style={styles.title}>Reminder</Text>
						<Text style={styles.slogan}>
							Stockez toutes vos idées au même endroit
						</Text>
						<View style={styles.form}>
							<Text style={styles.label}>Adresse Email</Text>
							<Controller
								control={control}
								render={({ field: { value, onChange } }) => (
									<TextInput
										style={styles.input}
										placeholder='votre email'
										keyboardType='email-address'
										value={value}
										onChangeText={(value) => onChange(value)}
									/>
								)}
								name='email'
								rules={{
									required: {
										value: true,
										message: 'Vous devez renseigner votre adresse Email.',
									},
									pattern: {
										value:
											/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
										message: 'Vous devez renseigner un adresse Email valide.',
									},
								}}
							/>
							{errors.email && (
								<Text style={{ ...styles.error }}>{errors.email.message}</Text>
							)}
							<Text style={{ ...styles.label, marginTop: 15 }}>
								Mot de passe
							</Text>
							<Controller
								control={control}
								render={({ field: { value, onChange } }) => (
									<TextInput
										style={styles.input}
										placeholder='votre mot de passe'
										secureTextEntry={true}
										value={value}
										onChangeText={(value) => onChange(value)}
									/>
								)}
								name='password'
								rules={{
									required: {
										value: true,
										message: 'Vous devez renseigner votre mot de passe.',
									},
									minLength: {
										value: 6,
										message:
											'Votre mot de passe doit faire plus de 6 caractères.',
									},
								}}
							/>
							{errors.password && (
								<Text style={{ ...styles.error }}>
									{errors.password.message}
								</Text>
							)}
						</View>
						<TouchableOpacity
							activeOpacity={0.8}
							onPress={handleSubmit(onSubmit)}>
							<View style={styles.mainButton}>
								<Text style={styles.mainButtonText}>
									{loginMode ? 'Connexion' : 'Inscription'}
								</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity
							activeOpacity={0.8}
							onPress={() => setLoginMode((prevState) => !prevState)}>
							<View>
								<Text style={styles.switchButtonText}>
									{loginMode
										? "Passer à l'inscription"
										: 'Passer à la connexion'}
								</Text>
							</View>
						</TouchableOpacity>
					</View>
				</SafeAreaView>
			</View>
		</KeyboardAvoidingView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: Colors.primary,
	},
	littleContainer: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		fontSize: 30,
		color: 'white',
		textTransform: 'uppercase',
		fontWeight: 'bold',
	},
	slogan: {
		color: 'white',
		paddingHorizontal: 15,
		fontSize: 16,
	},
	input: {
		padding: 8,
		backgroundColor: Colors.secondary,
		borderRadius: 8,
		width: '100%',
	},
	form: {
		marginTop: 30,
		backgroundColor: Colors.backForm,
		padding: 30,
		borderRadius: 5,
		width: Dimensions.get('window').width * 0.7,
	},
	label: {
		marginBottom: 5,
		color: 'white',
	},
	mainButton: {
		backgroundColor: Colors.loginButton,
		paddingHorizontal: 30,
		paddingVertical: 10,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 10,
		marginTop: 30,
	},
	mainButtonText: {
		color: 'white',
		fontSize: 16,
	},
	switchButtonText: {
		color: 'white',
		marginTop: 15,
	},
	error: {
		color: Colors.primary,
		marginTop: 5,
	},
});
