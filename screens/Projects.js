// Librairies
import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	SafeAreaView,
	Platform,
	Image,
	TouchableOpacity,
	FlatList,
	Alert,
} from 'react-native';
import Colors from '../constants/Colors';
import { LinearGradient } from 'expo-linear-gradient';
import * as appActions from '../store/actions/app';
import Ionicons from 'react-native-vector-icons/Ionicons';

// Redux
import { useSelector, useDispatch } from 'react-redux';

export default function Project(props) {
	// Variables
	const projects = useSelector((state) => state.projects);
	const notes = useSelector((state) => state.notes);
	const userId = useSelector((state) => state.userId);
	const token = useSelector((state) => state.token);

	// Variables
	const dispatch = useDispatch();

	// Fonctions
	const onLongPressHandler = (projectId) => {
		Alert.alert('Que souhaitez vous faire?', undefined, [
			{ text: 'Annuler', style: 'cancel' },
			{
				text: 'Supprimer',
				style: 'destructive',
				onPress: () => {
					onDeleteHandler(projectId);
				},
			},
		]);
	};

	const onDeleteHandler = (projectId) => {
		Alert.alert(
			'Attention',
			'Vous allez supprimer ce projet, en êtes vous sûre?',
			[
				{ text: 'Annuler', style: 'cancel' },
				{
					text: 'Supprimer',
					style: 'destructive',
					onPress: () => {
						const notesForProject = notes.filter(
							(note) => note.projectId === projectId
						);
						notesForProject.forEach((note) =>
							dispatch(appActions.deleteNote(note.id, userId, token))
						);
						dispatch(appActions.deleteProject(projectId, userId, token));
					},
				},
			]
		);
	};

	const onLogoutPressedHandler = () => {
		dispatch(appActions.logout());
	};

	return (
		<View style={styles.container}>
			<SafeAreaView style={{ flex: 1 }}>
				<View style={styles.containerProject}>
					<Text style={styles.title}>Projets</Text>
					{projects[0] && (
						<TouchableOpacity
							activeOpacity={0.8}
							onPress={() => props.navigation.navigate('addProject')}>
							<View style={styles.smallAddButton}>
								<Text style={styles.smallAddButtonText}>Ajouter</Text>
							</View>
						</TouchableOpacity>
					)}
				</View>

				{!projects[0] ? (
					<View style={styles.emptyProjects}>
						<Image
							source={require('../assets/folder.png')}
							style={styles.illustration}
						/>
						<Text>Commencez par créer votre premier projet.</Text>
						<TouchableOpacity
							activeOpacity={0.8}
							onPress={() => props.navigation.navigate('addProject')}>
							<LinearGradient
								colors={['#A996F2', '#8F79FC']}
								style={styles.addButton}>
								<Text style={styles.addButtonText}> Créer un projet </Text>
							</LinearGradient>
						</TouchableOpacity>
					</View>
				) : (
					<FlatList
						data={projects}
						renderItem={({ item }) => (
							<TouchableOpacity
								activeOpacity={0.8}
								onPress={() =>
									props.navigation.navigate('Project', { item: item })
								}
								onLongPress={() => {
									onLongPressHandler(item.id);
								}}>
								<View style={styles.project}>
									<Text style={styles.projectText}> {item.name} </Text>
								</View>
							</TouchableOpacity>
						)}
					/>
				)}
			</SafeAreaView>
			<TouchableOpacity activeOpacity={0.8} onPress={onLogoutPressedHandler}>
				<View style={styles.smallAddButton}>
					<Ionicons name='power' size={20} color={'white'} />
				</View>
			</TouchableOpacity>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: Colors.secondary,
		paddingHorizontal: 25,
	},
	containerProject: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	title: {
		fontSize: 30,
		fontWeight: 'bold',
		marginBottom: 30,
		marginTop: Platform.OS === 'android' ? 50 : 30,
	},
	illustration: {
		width: 150,
		height: 150,
	},
	emptyProjects: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	addButton: {
		padding: 10,
		borderRadius: 5,
		marginTop: 30,
		alignItems: 'center',
	},
	addButtonText: {
		fontSize: 18,
		color: 'white',
	},
	project: {
		backgroundColor: Colors.primaryFaded,
		padding: 15,
		marginBottom: 15,
		borderRadius: 15,
	},
	projectText: {
		fontSize: 17,
	},
	smallAddButton: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 30,
		width: 70,
		borderRadius: 15,
		backgroundColor: Colors.primary,
		marginBottom: 13,
		marginTop: Platform.OS === 'android' ? 50 : 30,
	},
	smallAddButtonText: {
		color: 'white',
	},
});
