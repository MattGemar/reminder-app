// Librairies
import React from 'react';
import {
	AppModalsNavigator,
	StartupStackNavigator,
	AuthenticatorStackNavigator,
} from './Navigators';
import { NavigationContainer } from '@react-navigation/native';
import { useSelector } from 'react-redux';

export default function AppNavigator(props) {
	// Variable
	const didTrialAutoLogin = useSelector((state) => state.didTrialAutoLogin);
	const isAuth = !!useSelector((state) => state.userId);
	return (
		<NavigationContainer>
			{didTrialAutoLogin && !isAuth && <AuthenticatorStackNavigator />}
			{didTrialAutoLogin && isAuth && <AppModalsNavigator />}
			{!didTrialAutoLogin && <StartupStackNavigator />}
		</NavigationContainer>
	);
}
