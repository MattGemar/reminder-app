import {
	ADD_PROJECT,
	ADD_NOTE,
	GET_NOTES,
	GET_PROJECTS,
	DELETE_NOTE,
	DELETE_PROJECT,
	START_GET_NOTES,
	END_GET_NOTES,
	START_GET_PROJECTS,
	END_GET_PROJECTS,
	SIGNUP,
	SET_TRIAL_LOGIN,
	FETCH_REFRESH_TOKEN,
	LOGOUT,
	LOGIN,
} from '../actions/app';
import moment from 'moment';

const initialState = {
	notes: [],
	projects: [],
	loadedNotes: false,
	loadedProjects: false,
	userId: null,
	token: null,
	didTrialAutoLogin: false,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case ADD_PROJECT:
			return {
				...state,
				projects: [action.project, ...state.projects],
			};
		case ADD_NOTE:
			return {
				...state,
				notes: [action.note, ...state.notes],
			};
		case GET_NOTES:
			const fetchedNotes = action.notes;
			fetchedNotes.sort(function (a, b) {
				let dateA = moment(a.creationDate);
				let dateB = moment(b.creationDate);
				return dateB - dateA;
			});

			return {
				...state,
				notes: fetchedNotes,
			};
		case GET_PROJECTS:
			const fetchedProjects = action.projects;
			return {
				...state,
				projects: fetchedProjects,
			};
		case DELETE_NOTE:
			let actualNotes = [...state.notes];
			actualNotes = actualNotes.filter((note) => note.id !== action.noteId);

			return {
				...state,
				notes: [...actualNotes],
			};
		case DELETE_PROJECT:
			let actualProjects = [...state.projects];
			actualProjects = actualProjects.filter(
				(project) => project.id !== action.projectId
			);
			return {
				...state,
				projects: [...actualProjects],
			};
		case START_GET_NOTES:
			return {
				...state,
				loadedNotes: true,
			};
		case END_GET_NOTES:
			return {
				...state,
				loadedNotes: false,
			};
		case START_GET_PROJECTS:
			return {
				...state,
				loadedProjects: true,
			};
		case END_GET_PROJECTS:
			return {
				...state,
				loadedProjects: false,
			};
		case SIGNUP:
			return {
				...state,
				userId: action.userId,
				token: action.token,
			};
		case SET_TRIAL_LOGIN:
			return {
				...state,
				didTrialAutoLogin: true,
			};
		case FETCH_REFRESH_TOKEN:
			return {
				...state,
				didTrialAutoLogin: true,
				userId: action.userId,
				token: action.token,
			};
		case LOGIN:
			return {
				...state,
				userId: action.userId,
				token: action.token,
			};
		case LOGOUT:
			return {
				...state,
				userId: null,
				token: null,
			};
		default:
			return state;
	}
};
