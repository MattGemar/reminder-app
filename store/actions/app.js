import axios from '../../axios-instance';
import Keys from '../../constants/Keys';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const ADD_PROJECT = 'ADD_PROJECT';
export const ADD_NOTE = 'ADD_NOTE';
export const GET_NOTES = 'GET_NOTES';
export const GET_PROJECTS = 'GET_PROJECTS';
export const DELETE_NOTE = 'DELETE_NOTE';
export const DELETE_PROJECT = 'DELETE_PROJECT';
export const START_GET_NOTES = 'START_GET_NOTES';
export const END_GET_NOTES = 'END_GET_NOTES';
export const START_GET_PROJECTS = 'START_GET_PROJECTS';
export const END_GET_PROJECTS = 'END_GET_PROJECTS';
export const SIGNUP = 'SIGNUP';
export const SET_TRIAL_LOGIN = 'SET_TRIAL_LOGIN';
export const FETCH_REFRESH_TOKEN = 'FETCH_REFRESH_TOKEN';
export const LOGOUT = 'LOGOUT';
export const LOGIN = 'LOGIN';

export const addProject = (project, userId, token) => {
	return (dispatch) => {
		axios
			.post('/projects/' + userId + '.json?auth=' + token, project)
			.then((response) => {
				const newProject = {
					id: response.data.name,
					name: project.name,
					logo: project.logo,
				};
				dispatch({ type: ADD_PROJECT, project: newProject });
			})
			.catch((error) => {
				console.log(error);
			});
	};
};

export const addNote = (note, userId, token) => {
	return (dispatch) => {
		axios
			.post('/notes/' + userId + '.json?auth=' + token, note)
			.then((response) => {
				const newNote = {
					id: response.data.name,
					projectId: note.projectId,
					content: note.content,
					creationDate: note.creationDate,
				};
				console.log(newNote);
				dispatch({ type: ADD_NOTE, note: newNote });
			})
			.catch((error) => {
				console.log(error);
			});
	};
};

export const getNotes = (userId, token) => {
	return (dispatch) => {
		dispatch({ type: START_GET_NOTES });
		axios
			.get('/notes/' + userId + '.json?auth=' + token)
			.then((response) => {
				const fetchedNotes = [];
				console.log(response.data);
				for (let key in response.data) {
					fetchedNotes.push({
						id: key,
						projectId: response.data[key].projectId,
						content: response.data[key].content,
						creationDate: response.data[key].creationDate,
					});
				}
				dispatch({ type: GET_NOTES, notes: fetchedNotes });
				dispatch({ type: END_GET_NOTES });
			})
			.catch((error) => {
				console.log(error);
				dispatch({ type: END_GET_NOTES });
			});
	};
};

export const getProjects = (userId, token) => {
	return (dispatch) => {
		dispatch({ type: START_GET_PROJECTS });
		axios
			.get('/projects/' + userId + '.json?auth=' + token)
			.then((response) => {
				const fetchedProjects = [];
				for (let key in response.data) {
					fetchedProjects.push({
						id: key,
						name: response.data[key].name,
						logo: response.data[key].logo,
					});
				}
				dispatch({ type: GET_PROJECTS, projects: fetchedProjects });
				dispatch({ type: END_GET_PROJECTS });
			})
			.catch((error) => {
				console.log(error);
				dispatch({ type: END_GET_PROJECTS });
			});
	};
};

export const deleteNote = (noteId, userId, token) => {
	return (dispatch) => {
		axios
			.delete('/notes/' + userId + '/' + noteId + '.json?auth=' + token)
			.then((response) => {
				console.log(response);
				dispatch({ type: DELETE_NOTE, noteId: noteId });
			})
			.catch((error) => {
				console.log(error);
			});
	};
};

export const deleteProject = (projectId, userId, token) => {
	return (dispatch) => {
		axios
			.delete('/projects/' + userId + '/' + projectId + '.json?auth=' + token)
			.then((response) => {
				console.log(response);
				dispatch({ type: DELETE_PROJECT, projectId: projectId });
			})
			.catch((error) => {
				console.log(error);
			});
	};
};

export const fetchRefreshToken = (refreshToken) => {
	return (dispatch) => {
		axios
			.post(
				'https://securetoken.googleapis.com/v1/token?key=' + Keys.firebase,
				{
					refreshToken: refreshToken,
					grantType: 'refresh_token',
				}
			)
			.then((response) => {
				console.log(response.data);
				dispatch({
					type: FETCH_REFRESH_TOKEN,
					token: response.data.id_token,
					refreshToken: response.data.refresh_token,
					userId: response.data.user_id,
				});
				saveDataToStorage(response.data.id_token, response.data.refresh_token);
			})
			.catch((error) => {
				console.log(error);
			});
	};
};

export const signup = (email, password) => {
	return async (dispatch) => {
		await axios
			.post(
				'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=' +
					Keys.firebase,
				{
					email: email,
					password: password,
					returnSecureToken: true,
				}
			)
			.then((response) => {
				saveDataToStorage(response.data.idToken, response.data.refreshToken);
				dispatch({
					type: SIGNUP,
					userId: response.data.localId,
					token: response.data.idToken,
				});
			})
			.catch((error) => {
				throw new Error(error.response.data.error.message);
			});
	};
};

export const login = (email, password) => {
	return async (dispatch) => {
		await axios
			.post(
				'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' +
					Keys.firebase,
				{
					email: email,
					password: password,
					returnSecureToken: true,
				}
			)
			.then((response) => {
				saveDataToStorage(response.data.idToken, response.data.refreshToken);
				dispatch({
					type: LOGIN,
					userId: response.data.localId,
					token: response.data.idToken,
				});
			})
			.catch((error) => {
				throw new Error(error.response.data.error.message);
			});
	};
};

export const setDidTrial = () => {
	return {
		type: SET_TRIAL_LOGIN,
	};
};

export const logout = () => {
	AsyncStorage.removeItem('userData');
	return {
		type: LOGOUT,
	};
};

const saveDataToStorage = (token, refreshToken) => {
	AsyncStorage.setItem(
		'userData',
		JSON.stringify({
			token: token,
			refreshToken: refreshToken,
		})
	);
};
