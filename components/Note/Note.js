// Librairies
import React, { useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native';
import Colors from '../../constants/Colors';
import * as appActions from '../../store/actions/app';

// Redux
import { useDispatch, useSelector } from 'react-redux';

export default function Note(props) {
	// Variables
	const dispatch = useDispatch();
	const userId = useSelector((state) => state.userId);
	const token = useSelector((state) => state.token);

	// Fonctions
	const onLongPressHandler = () => {
		Alert.alert('Que souhaitez vous faire?', undefined, [
			{ text: 'Annuler', style: 'cancel' },
			{
				text: 'Supprimer',
				style: 'destructive',
				onPress: () => onDeleteHandler(),
			},
		]);
	};

	const onDeleteHandler = () => {
		Alert.alert(
			'Attention',
			'Vous allez supprimer cette note, en êtes vous sûre?',
			[
				{ text: 'Annuler', style: 'cancel' },
				{
					text: 'Supprimer',
					style: 'destructive',
					onPress: () =>
						dispatch(appActions.deleteNote(props.item.id, userId, token)),
				},
			]
		);
	};

	return (
		<TouchableOpacity activeOpacity={0.8} onLongPress={onLongPressHandler}>
			<View style={styles.note}>
				<Text>{props.item.content}</Text>
			</View>
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	note: {
		backgroundColor: 'white',
		padding: 15,
		borderColor: Colors.primary,
		borderWidth: 1,
		borderRadius: 5,
		marginBottom: 15,
	},
});
